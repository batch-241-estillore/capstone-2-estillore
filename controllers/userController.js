
const User = require("../models/User");
const userRoute = require("../routes/userRoute");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");


module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then( result => {

		if(result.length > 0){
			return true
		}else{
			return false
		}
	})
}

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		name: reqBody.name,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user,err) => {
		if(err){
			return false
		} else {
			return user
		}
	})
}


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		} else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return { accessToken: auth.createAccessToken(result)}
			} else {
				return false
			};
		}
	})
}

module.exports.getAllUsers = () => {
	return User.find().then(result => {return result});
}

function search(model, dataId) {
	return model.findById(dataId).then(result => {return result});
}



module.exports.checkout = async (data) => {

	let userToBeUpdated = search(User, data.userId);

	let productToBeUpdated = search(Product, data.productId);
	let user = await userToBeUpdated;
	let product = await productToBeUpdated;
	
	if(user.id == data.userId && product.id == data.productId){

		let isUserUpdated = () => {
			user.orders.push(
			{
				products: [
				{
					productName: product.name,
					quantity: data.quantity
				}
				],
				totalAmount: (data.quantity*product.price)
			}

			);
			console.log(user);

			return user.save().then((user,err) => {
				if(err){
					return false
				} else {
					console.log(user)
					return true
				}
			});
		}
		isUserUpdated();

		let isProductUpdated = () => {
			product.orders.push({
				userId: data.userId,
				userName: user.name
			});

			return product.save().then((product, err) => {
				if(err){
					return false
				} else {

					return true
				}
			});
		}
		isProductUpdated();
		if(isUserUpdated && isProductUpdated){

			return true
		} else {
			return false
		};

	}
	
}


module.exports.getDetails = (userData) => {

	return User.findById(userData.id).then(result => {
		if(result == null){
			return false
		} else{
			result.password = "";
			return result
		}
	})
}


module.exports.makeAdmin = (reqBody) => {

	return User.findById(reqBody.userId).then(result => {
		console.log(reqBody)
		if(result==null){
			return false	
		}else{
			if(result.isAdmin){
				return false
			}else{
				result.isAdmin = true
				return result.save().then((result,err) => {
					if(err){
						return false
					} else {
						return true
					}
				})
			}	
		}
		
	})
}


module.exports.getMyOrders = (userId) => {
	console.log(userId)
	return User.findById(userId).then(result => {
		if(result == null){
			return ("No match")
		} else {
			return result.orders;
		}
	})
}
