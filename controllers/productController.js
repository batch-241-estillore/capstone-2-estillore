
const Product = require("../models/Product");
const productRoute = require("../routes/productRoute");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		img: reqBody.img
	})
	return newProduct.save().then((product, err) => {
		if(err){
			return false
		} else {
			return true
		}
	})
}

module.exports.checkNameExists = (reqBody) => {

	return Product.find({name: reqBody.name}).then( result => {

		if(result.length > 0){
			return true
		}else{
			return false
		}
	})
}

module.exports.getAllProducts = () => {
	return Product.find({isActive: true}).then(result => {return result});
}

module.exports.getProductsAdmin = () => {
	return Product.find().then(result => {return result});
}

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then((result) => {
		return result;
	});
};

module.exports.updateProduct = (reqBody, reqParams) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		img: reqBody.img
	}
	//try catching non-updates; maybe use async
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, err) => {
		if(err){
			return false;
		} else {
			return true;
		}
	});

}




//try denying change if there is no change
module.exports.archiveProduct = (reqBody, reqParams) => {
	let archivedProduct = {
		isActive: reqBody.isActive
	}
	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((result, err) => {
		if(err){
			return false;
		} else {
			return true;
		}
	});


}
