const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userController");

router.post("/checkEmail", (req,res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});



router.get("/allUsers", (req,res) => {
	userController.getAllUsers().then(resultFromController => {
		res.send(resultFromController)
	})
});


router.post("/checkout", auth.verify, (req,res) => {
	let access = auth.decode(req.headers.authorization);
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	};
	if(!access.isAdmin){
		userController.checkout(data).then(resultFromController => res.send(resultFromController));	
	} else {
		return res.send(false)
	}
	
});


router.get("/details", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);

	userController.getDetails({id: userData.id}).then(resultFromController => res.send(resultFromController))

})

router.patch("/adminUpgrade", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		userController.makeAdmin(req.body).then(resultFromController => res.send(resultFromController));	
	}else{
		return res.send(false)
	}
	
})


router.get("/myOrders", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	if(!userData.isAdmin)
	{
		userController.getMyOrders(userData.id).then(resultFromController => res.send(resultFromController));
	} else {
		return res.send("You are not a registered user")
	}
})


module.exports = router;