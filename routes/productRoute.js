const express = require("express");
const router = express.Router();
const auth = require("../auth");

const productController = require("../controllers/productController");


router.post("/addProduct", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		return res.send(`Product creation failed; you are not an admin`)
	}
})


router.post("/checkProductName", (req,res) => {

	productController.checkNameExists(req.body).then(resultFromController => res.send(resultFromController));
});


router.get("/allProducts", (req,res) => {
	productController.getAllProducts().then(resultFromController => {
		res.send(resultFromController)
	})
});

router.get("/allProductsAdmin", (req,res) => {
	productController.getProductsAdmin().then(resultFromController => {
		res.send(resultFromController)
	})
});

router.get("/:productId", (req,res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.patch("/update/:productId", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);

	
	if(userData.isAdmin){
		productController.updateProduct(req.body, req.params).then(resultFromController => {
			res.send(resultFromController)
		})
	} else {
		return res.send(`Product update failed; you are not an admin`)
	}
	
})

router.patch("/archive/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.archiveProduct(req.body, req.params).then(resultFromController => {
			res.send(resultFromController)
		})
	} else {
		return false
	}
})







module.exports = router;