const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	img: {
		type: String
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			userId: {
					type: String,
					required: [true, "Users are required"]
			},
			userName: {
				type: String,
				required: [true, "User's name is required"]
			}
		}
	]
	
});

module.exports = mongoose.model("Product", productSchema);