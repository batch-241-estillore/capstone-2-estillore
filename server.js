const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const port = 4040;
//require routessssss
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");



const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.sw01nc7.mongodb.net/capstone-2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})


let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the cloud database"));

//app.use routessss
app.use("/users", userRoute);
app.use("/products", productRoute);

app.listen(port, () => console.log(`API is now online on port ${port}`))